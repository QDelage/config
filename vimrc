set clipboard=unnamedplus
set go+=a               " Visual selection automatically copied to the clipboard
set whichwrap=b,s,<,>,[,]
syntax on
set mouse=a

" change leader
let mapleader = ","

" tab size
set tabstop=4 softtabstop=4
set shiftwidth=4 smarttab
set expandtab
set autoindent smartindent

" Disable auto fold
set nofoldenable

" Pour couper l'auto indent pour paste
set pastetoggle=<F10>

" Ctrl + Arrow
execute "set <xUp>=\e[1;*A"
execute "set <xDown>=\e[1;*B"
execute "set <xRight>=\e[1;*C"
execute "set <xLeft>=\e[1;*D"

let g:rainbow_active = 1 "set to 0 if you want to enable it later via :RainbowToggle

" Pour clean LaTeX preview
let g:latex_preview_clean = 1

" Pour faciliter les ctags
set autochdir " Va remonter les directories pour trouver le fichier tags

" Pour les numéros de ligne
set number

" Pour Grammalecte
let g:grammalecte_cli_py='/usr/bin/grammalecte-cli.py'

" Touche 'm' pour appeller make silencieusement
map m :call system('make &')<CR>

" Touche 'g' pour appeller GrammalecteCheck 
"map g :GrammalecteCheck<CR>

" Ouvrir les vsplit à droite, et split en bas
set splitright
set splitbelow

" ALE (syntax checker) no warnings for LaTeX, 1: command terminated with
" space, 26: whitespaces before ;
let g:ale_tex_chktex_options='-I --nowarn 1 --nowarn 26'

" Highlight searches
set hlsearch

" Ignore case on searches
set ignorecase
set smartcase

" Wrapped lines will stay indented the same
set wrap breakindent

" To keep undo history in a file
set undofile

" To read if the file has been changed on disk but not internally
set autoread

" To wrap at a word and not in the middle of one
set linebreak

" Better (apparently) completion menu
set wildmenu
"set wildmode=list:longest
set wildmode=longest:list,full

" Better colors
" Can use :colorscheme to change theme
set termguicolors

"colorscheme torte
colorscheme desert
set background=dark

" Resize splits when the window is resized
au VimResized * :wincmd =

" setting fonts
set gfn=IBM\ Plex\ Mono\ 14,:Hack\ 14,Source\ Code\ Pro\ 12,Bitstream\ Vera\ Sans\ Mono\ 11

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Turn persistent undo on 
"    means that you can undo even when you close a buffer/VIM
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
try
    set undodir=~/.vim_runtime/temp_dirs/undodir
    set undofile
catch
endtry

" :W sudo saves the file
" (useful for handling the permission-denied error)
command! W execute 'w !sudo tee % > /dev/null' <bar> edit!

" Don't redraw while executing macros (good performance config)
set lazyredraw

" Show matching brackets when text indicator is over them
set showmatch

" Linebreak on 500 characters
set lbr
set tw=500

" Disable highlight when space is pressed
map <silent> <space> :noh<cr>

" Return to last edit position when opening files (You want this!)
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Pressing ,ss will toggle and untoggle spell checking
map <leader>ss :setlocal spell!<cr>
set spell spelllang=fr
set spell! " deactivate spell check by default

" Shortcuts for spell checking using <leader>
map <leader>sn ]s
map <leader>sp [s
map <leader>sa zg
map <leader>s? z=




"=================================================================================
"
"   Following file contains the commands on how to run the currently open code.
"   The default mapping is set to F5 like most code editors.
"   Change it as you feel comfortable with, keeping in mind that it does not
"   clash with any other keymapping.
"
"   Just add another elseif block before the 'endif' statement in the same
"   way it is done in each case. Take care to add tabbed spaces after each
"   elseif block (similar to python).
"
"   NOTE: The '%' sign indicates the name of the currently open file with extension.
"         The time command displays the time taken for execution. Remove the
"         time command if you dont want the system to display the time
"
"=================================================================================

map <F5> :call CompileRun()<CR>
imap <F5> <Esc>:call CompileRun()<CR>
vmap <F5> <Esc>:call CompileRun()<CR>

func! CompileRun()
exec "w"
if &filetype == 'c'
    exec "!gcc % -o %<"
    exec "!time ./%<"
elseif &filetype == 'cpp'
    exec "!g++ % -o %<"
    exec "!time ./%<"
elseif &filetype == 'java'
    exec "!javac %"
    exec "!time java %"
elseif &filetype == 'sh'
    exec "!time bash %"
elseif &filetype == 'python'
    exec "!time python3 -m pudb %"
elseif &filetype == 'html'
    exec "!google-chrome % &"
elseif &filetype == 'go'
    exec "!go build %<"
    exec "!time go run %"
elseif &filetype == 'matlab'
    exec "!time octave %"
endif
endfunc

"=================================================================================

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Helper functions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Returns true if paste mode is enabled
function! HasPaste()
    if &paste
        return 'PASTE MODE  '
    endif
    return ''
endfunction

"=================================================================================


" To add a header in markdown file, just use ,h (where , is the current leader)
nnoremap <leader>h gg<ESC>ggO<ESC>:normal i---<ESC>o<ESC>otitle: <C-R>=expand("%:t:r")<ESC><ESC>odate: <C-R>=strftime("%Y-%m-%d")<ESC><ESC>oauthor: qde<ESC>o<ESC>o---<ESC>o<ESC>o<ESC>j0i<BS><ESC>l




" PlugInstall
call plug#begin('~/.vim/plugged')
    " Make sure you use single quotes

    " Background
    Plug 'junegunn/seoul256.vim'

    " A simple, easy-to-use Vim alignment plugin
    Plug 'junegunn/vim-easy-align'

    " Group dependencies, vim-snippets depends on ultisnips
    Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'

    " Plugin outside ~/.vim/plugged with post-update hook
    Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': 'yes \| ./install' }

    Plug 'vim-airline/vim-airline'

    " My plugins:
    " Rainbow parenthesis
    Plug 'luochen1990/rainbow'

    " Pour commenter
    Plug 'preservim/nerdcommenter'

    " Vim markdown :Toc
    Plug 'preservim/vim-markdown'
    
    "Plug 'vim-syntastic/syntastic'

    " syntax etc checker, async
    Plug 'dense-analysis/ale'

    " Pour commenter avec gc et gcc
    Plug 'tpope/vim-commentary'

    " Completion
    Plug 'ycm-core/YouCompleteMe', { 'commit': '07e3ae94c3ccf3103a859f2321de4d4a2a64ac49' }

    " Git
    Plug 'airblade/vim-gitgutter'

    " Grammalecte
    Plug 'dpelle/vim-Grammalecte'

    " LaTeX Preview with :StartLatexPreview
    "Plug 'conornewton/vim-latex-preview'

    " Generate python docstring
    Plug 'heavenshell/vim-pydocstring', { 'do': 'make install', 'for': 'python' }
 
    " Markdown preview
    Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']} 

    " Sidebar directories
    Plug 'preservim/nerdtree'

    " Indent rainbow
    Plug 'adi/vim-indent-rainbow'

    " Kotlin colours
    Plug 'udalov/kotlin-vim'

call plug#end()

