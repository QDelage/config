#!/bin/bash

# TODO: apply special rules for IPv6 required packets

# Apply equal rules to IPv4 and IPv6 firewalls
for fw in iptables ip6tables
do

# Remove all previous
$fw -t filter -F
$fw -t nat -F

# Policies
$fw -P INPUT DROP
$fw -P FORWARD DROP
$fw -P OUTPUT ACCEPT

# Authorize input from established connexions
$fw -A INPUT -m conntrack --ctstate ESTABLISHED -j ACCEPT

# Allow loopback
$fw -A INPUT -i lo -j ACCEPT

# Block the mute group from networking
$fw -t filter -A OUTPUT -m owner --gid-owner $(getent group mute | cut -d: -f3) -j DROP

# Log the log group packets
$fw -t filter -A OUTPUT -m owner --gid-owner $(getent group log | cut -d: -f3) -j LOG --log-prefix='[netfilter] '

done

