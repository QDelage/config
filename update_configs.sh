#!/bin/bash
cp $HOME/.vimrc vimrc
cp $HOME/.zshrc zshrc
cp $HOME/.aliases aliases
cp $HOME/.tmux.conf tmux.conf

