#!/bin/bash

cp $HOME/.vimrc $HOME/.vimrc.bk
cp $HOME/.zshrc $HOME/.zshrc.bk
cp $HOME/.aliases $HOME/.aliases.bk
cp $HOME/.tmux.conf $HOME/.tmux.conf.bk

cp vimrc $HOME/.vimrc
cp zshrc $HOME/.zshrc
cp aliases $HOME/.aliases
cp tmux.conf $HOME/.tmux.conf

